// import { PAGE_CHANGE_PAGINATION, USER_FETCH_ERROR, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/user.constants"

import { ON_PAGE_NUMBER_CHANGE, OPEN_MODAL, PAGE_CHANGE_PAGINATION, USER_FETCH_ERROR, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/user.constant"

export const fetchUser = (page, size, numberPage, noColOfPage) => {
    return async (dispatch) => {
        try {
            var requestOptions = {
                method: "GET"
            }
            await dispatch({
                type: USER_FETCH_PENDING
            })
            // Gọi để lấy tổng số users có trong CSDL
            const responseUsers = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/user-list-pagination", requestOptions);

            const dataUsers = await responseUsers.json();

            const params = new URLSearchParams({
                'page': (page - 1) * size,
                'size': size
            })

            const responsePaginationUsers = await fetch("http://203.171.20.210:8080/devcamp-register-java-api/user-list-pagination?" + params.toString(), requestOptions);

            const dataPagintionUsers = await responsePaginationUsers.json();


            return dispatch({
                type: USER_FETCH_SUCCESS,
                totalUser: dataUsers.length,
                data: dataPagintionUsers,
                numberPage,
                noColOfPage
            })

        } catch (error) {
            return dispatch({
                type: USER_FETCH_ERROR,
                error: error
            })
        }
    }
}
export const userModalOpen = (payload) => {
    return ({
        type: OPEN_MODAL,
        payload

    })
}

export const pageChangePagination = (page) => {
    return {
        type: PAGE_CHANGE_PAGINATION,
        payload: page
    }
}
export const pageNumberChange = (noPage) => {
    return {
        type: ON_PAGE_NUMBER_CHANGE,
        payload: noPage
    }
}
