import { act } from "react-dom/test-utils";
import { ON_PAGE_NUMBER_CHANGE, OPEN_MODAL, PAGE_CHANGE_PAGINATION, USER_FETCH_PENDING, USER_FETCH_SUCCESS } from "../constants/user.constant";

const initialState = {
    users: [],
    pending: false,
    userModal: {},
    size: 2,
    noPage: 0,
    currentPage: 1
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_FETCH_PENDING:
            state.pending = true
            break;
        case USER_FETCH_SUCCESS:
            state.pending = false
            state.noPage = state.noPage === 0 ? Math.ceil(action.totalUser / state.size) : action.payload.numberPage
            // if (state.noPage === 10) {
            //     state.noPage = 10
            // } else {
            //     state.noPage = action.payload.numberPage
            // }
            state.users = action.data
            break;
        case OPEN_MODAL:
            state.userModal = action.payload
            break;
        case ON_PAGE_NUMBER_CHANGE:
            state.noPage = action.payload;
            break;

        case PAGE_CHANGE_PAGINATION:
            state.currentPage = action.payload;
            break;

        default:
            break;
    }


    return { ...state }
}