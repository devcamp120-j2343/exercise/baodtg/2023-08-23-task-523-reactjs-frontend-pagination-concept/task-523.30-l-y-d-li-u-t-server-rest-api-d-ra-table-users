export const USER_FETCH_PENDING = "Trạng thái đợi khi gọi API lấy danh sách User";

export const USER_FETCH_SUCCESS = "Trạng thái thành công khi gọi API lấy danh sách User"; 

export const USER_FETCH_ERROR = "Trạng thái lỗi khi gọi API lấy danh sách User"; 

export const PAGE_CHANGE_PAGINATION = "Sự kiện thay đổi trang";

export const OPEN_MODAL = "Sự kiện hiển thị modal thông tin user khi ấn nút Chi Tiết";

export const ON_PAGE_NUMBER_CHANGE = "Sự kiện thay đổi hiển thị tổng số trang";
