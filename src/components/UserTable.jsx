import { Box, Button, CircularProgress, Container, FormControl, Grid, InputLabel, MenuItem, Modal, Pagination, Paper, Select, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material"
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchUser, pageChangePagination, pageNumberChange, userModalOpen } from "../actions/user.actions";

let numberOfPage = 0;

export const UserTable = () => {
    const dispatch = useDispatch();

    const { users, pending, userModal, size, noPage, currentPage } = useSelector((reduxData) => reduxData.userReducer);

    useEffect(() => {

        dispatch(fetchUser(currentPage, size, numberOfPage))

    }, [currentPage])

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 400,
        bgcolor: 'background.paper',
        border: '2px solid #000',
        boxShadow: 24,
        p: 4,
    };

    const [open, setOpen] = React.useState(false);

    const handleOpen = (user) => {
        dispatch(userModalOpen(user))
        setOpen(true)
    };
    const handleClose = () => setOpen(false);

    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    }

    const onPageNumberChangeHandler = (event) => {
        numberOfPage = event.target.value;

        dispatch(pageNumberChange(event.target.value));

    };



    return (
        <>
            <Container >
                <Grid container mt={5} >
                    <Grid item lg={12} md={12} sm={12} xs={12}>

                        <Box sx={{ minWidth: 50 }}>
                            <FormControl sx={{ width: '200px' }}>
                                <InputLabel id="demo-simple-select-label">Select number page</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    label="Select number page"
                                    onChange={onPageNumberChangeHandler}
                                    value={"none"}
                                >
                                    <MenuItem value={5}>5</MenuItem>
                                    <MenuItem value={10}>10</MenuItem>
                                    <MenuItem value={25}>25</MenuItem>
                                    <MenuItem value={50}>50</MenuItem>
                                </Select>
                            </FormControl>
                        </Box>
                    </Grid>
                    {pending ?
                        <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
                            <CircularProgress color="secondary" />
                        </Grid>
                        :
                        <Grid item lg={12} md={12} sm={12} xs={12} >
                            <TableContainer component={Paper}>
                                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                                    <TableHead>
                                        <TableRow>
                                            <TableCell align="center">ID</TableCell>
                                            <TableCell align="left">First Name</TableCell>
                                            <TableCell align="left">Last Name</TableCell>
                                            <TableCell align="left">Country</TableCell>
                                            <TableCell align="left">Subject</TableCell>
                                            <TableCell align="left">Customer Type</TableCell>
                                            <TableCell align="left">Register Status</TableCell>
                                            <TableCell align="left">Action</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {users.map((user, index) => (
                                            <TableRow
                                                key={index}
                                                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                            >
                                                <TableCell align="center" component="th" scope="row">
                                                    {user.id}
                                                </TableCell>
                                                <TableCell align="left">{user.firstname}</TableCell>
                                                <TableCell align="left">{user.lastname}</TableCell>
                                                <TableCell align="left">{user.country}</TableCell>
                                                <TableCell align="left">{user.subject}</TableCell>
                                                <TableCell align="left">{user.customerType}</TableCell>
                                                <TableCell align="left">{user.registerStatus}</TableCell>
                                                <TableCell align="left"><Button onClick={() => { handleOpen(user) }}>Detail</Button></TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </Grid>
                    }

                </Grid>

                <Grid item lg={12} md={12} sm={12} xs={12} mt={5} mb={5} sx={{ display: "flex", justifyContent: "center" }}>
                    <Pagination count={noPage} page={currentPage} onChange={onChangePagination} color="secondary" />

                </Grid>


                <div>
                    <Modal
                        open={open}
                        onClose={handleClose}
                        aria-labelledby="modal-modal-title"
                        aria-describedby="modal-modal-description"
                    >
                        <Box sx={style}>
                            <Typography id="modal-modal-title"
                                variant="h6" component="h2">
                                THÔNG TIN NGƯỜI DÙNG
                            </Typography>
                            <Typography id="modal-modal-description"
                                sx={{ mt: 2 }}>
                                UserId: {userModal.id}
                            </Typography>
                            <Typography id="modal-modal-description"
                                sx={{ mt: 2 }}>
                                Firstname: {userModal.firstname}
                            </Typography>
                            <Typography id="modal-modal-description"
                                sx={{ mt: 2 }}>
                                Lastname: {userModal.lastname}
                            </Typography>
                            <Typography id="modal-modal-description"
                                sx={{ mt: 2 }}>
                                Country: {userModal.country}
                            </Typography>
                            <Typography id="modal-modal-description"
                                sx={{ mt: 2 }}>
                                Subject: {userModal.subject}
                            </Typography>
                            <Typography id="modal-modal-description"
                                sx={{ mt: 2 }}>
                                Customer Type: {userModal.customerType}
                            </Typography>
                            <Typography id="modal-modal-description"
                                sx={{ mt: 2 }}>
                                Register Status: {userModal.registerStatus}
                            </Typography>
                        </Box>
                    </Modal>
                </div>



            </Container >

        </>
    )
}
